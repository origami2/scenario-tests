function Plugin1(username, groups) {
  this.username = username;
  this.groups = groups;
}

Plugin1.prototype.echo = function (what) {
  return Promise.resolve(what);
}

Plugin1.prototype.getContext = function () {
  return Promise.resolve({
    username: this.username,
    groups: this.groups
  });
}

module.exports = Plugin1;
