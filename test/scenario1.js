var EventEmitter2 = require('eventemitter2').EventEmitter2;
var Stack = require('origami-stack');
var Plugin = require('origami-plugin');
var Plugin1 = require('./plugin1');
var TokenProvider = require('origami-token-provider');
var TokenProviderClient = require('origami-token-provider-client');
var TokenProvider1 = require('./tokenProvider1');
var Client = require('origami-client');
var testData = require('./testData');
var assert = require('assert');
var crypto = require('origami-crypto-utils');

describe('Scenario 1', function () {
  var stack;
  var stackSocket;
  var plugin1;
  var plugin1socket;
  var client1;
  var client1socket;
  var client2;
  var client2socket;
  var tokenProvider1;
  var tokenProvider1socket;
  var token1;
  var tokenProviderClient1;
  var tokenProviderClient1socket;

  before(function () {
    stackSocket = new EventEmitter2();
    plugin1socket = new EventEmitter2();
    client1socket = new EventEmitter2();
    client2socket = new EventEmitter2();
    tokenProvider1socket = new EventEmitter2();
    tokenProviderClient1socket = new EventEmitter2();
  });

  it('creates stack', function () {
    stack = new Stack(testData.stack.privateKey, testData.stack.publicKey);
  });

  it('creates plugin', function () {
    plugin1 = new Plugin(Plugin1);
  });

  it('plugin1 attaches to socket', function () {
    plugin1.listenSocket(plugin1socket);
  });

  it('stack attaches plugin socket', function () {
    stack.addPlugin(
      'Plugin1',
      plugin1socket,
      {
        echo: ['what'],
        'getContext': []
      }
    );

    assert.deepEqual({
      'Plugin1': {
        'echo': ['what'],
        'getContext': []
      }
    }, stack.listPlugins());
  });

  it('invokes plugin method', function (done) {
    stack
    .invokeMethod(null, {}, 'Plugin1', 'echo', { what: 'this' })
    .then(function (result) {
      try {
        assert.equal('this', result);

        done();
      } catch (e) {
        done(e);
      }
    })
    .catch(done);
  });

  it('creates token provider', function () {
    tokenProvider1 = new TokenProvider(TokenProvider1, testData.tokenProvider1.privateKey);
  });

  it('attaches to token provider socket', function () {
    tokenProvider1.listenStack(tokenProvider1socket, testData.stack.publicKey);
  });

  it('adds token provider socket', function () {
    stack
    .addTokenProvider(
      'TokenProvider1',
      tokenProvider1socket,
      {
        'userCredentials': ['username', 'password']
      },
      testData.tokenProvider1.publicKey
    );
  });

  it('gets token', function (done) {
    stack
    .getToken(
      null,
      'TokenProvider1',
      'userCredentials',
      {
        'username': 'user1',
        'password': 'secret'
      }
    )
    .then(function (token) {
      try {
        assert(token);

        var decrypted = crypto.decryptAndVerify(token, testData.stack.privateKey, testData.stack.publicKey)

        assert.deepEqual(
          {
            'username': 'user1',
            'groups': [
              'users',
              'developers'
            ]
          },
          decrypted
        );

        done();
      } catch (e) {
        done(e);
      }
    });
  });

  it('attaches to token provider client socket', function () {
    stack
    .addClient(tokenProviderClient1socket);
  });

  it('creates token provider client', function (done) {
    debugger;
    (new TokenProviderClient(tokenProviderClient1socket))
    .then(function (client) {
      try {
        assert(client);
        assert(client.TokenProvider1);
        assert(client.TokenProvider1.userCredentials);

        tokenProviderClient1 = client;

        done();
      } catch (e) {
        done(e);
      }
    })
  });

  it('gets token through token provider client', function (done) {
    tokenProviderClient1
    .TokenProvider1
    .userCredentials(
      'user1',
      'secret'
    )
    .then(function (token) {
      try {
        assert(token);

        var decrypted = crypto.decryptAndVerify(token, testData.stack.privateKey, testData.stack.publicKey)

        assert.deepEqual(
          {
            'username': 'user1',
            'groups': [
              'users',
              'developers'
            ]
          },
          decrypted
        );

        token1 = token;

        done();
      } catch (e) {
        done(e);
      }
    })
  });

  it('attaches client socket', function () {
    stack
    .addClient(client1socket);
  });

  it('creates client', function (done) {
    (new Client(client1socket))
    .then(function (client) {
      try {
        assert(client);

        client1 = client;

        done();
      } catch (e) {
        done(e);
      }
    })
  });

  it('returns call', function (done) {
    client1
    .Plugin1
    .echo('hello')
    .then(function (response) {
      try {
        assert.equal('hello', response);

        done();
      } catch (e) {
        done(e);
      }
    });
  });

  it('creates client with a token', function (done) {
    stack
    .addClient(client2socket);

    (new Client(client2socket, token1))
    .then(function (client) {
      try {
        assert(client);

        client2 = client;

        done();
      } catch (e) {
        done(e);
      }
    })
  });

  it('returns call', function (done) {
    client2
    .Plugin1
    .echo('hello')
    .then(function (response) {
      try {
        assert.equal('hello', response);

        done();
      } catch (e) {
        done(e);
      }
    });
  });

  it('passes token context', function (done) {
    client2
    .Plugin1
    .getContext()
    .then(function (context) {
      try {
        assert.deepEqual(
          {
            username: 'user1',
            'groups': [
              'users',
              'developers'
            ]
          },
          context
        );

        done();
      } catch (e) {
        done(e);
      }
    });
  });
});
