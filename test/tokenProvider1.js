function TokenProvider1() {
}

TokenProvider1.prototype.userCredentials = function (username, password) {
  if (username === 'user1' && password === 'secret') return Promise.resolve({
    'username': 'user1',
    'groups': [
      'users',
      'developers'
    ]
  });

  return Promise.reject('invalid credentials');
};

module.exports = TokenProvider1;
